package com.epam.jpop.apigateway.filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.stereotype.Component;

@Component
public class PreFilter extends ZuulFilter {

   private String filterType = "pre";
   private Integer filterOrder = 1;
   private Boolean shouldFilter = Boolean.TRUE;

   @Override
   public String filterType() {
      return filterType;
   }

   @Override
   public int filterOrder() {
      return filterOrder;
   }

   @Override
   public boolean shouldFilter() {
      return shouldFilter;
   }

   @Override
   public Object run() throws ZuulException {
      System.out.println("Executing pre filter");
      return null;
   }
}
